#!/usr/bin/python
# Purpose: Couple in to phisort.sh to give the trapezoidal integral of the data
# Author: Quinn A. Besford
# Email: quinn.besford@gmail.com
# Date: 17/01/2014

import sys
from scipy.integrate import trapz


import numpy

y = numpy.loadtxt('MoleResults.txt', delimiter =  ' ')


area = trapz(y, dx=.1)
print "The change in free energy is", area


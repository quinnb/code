#!/bin/bash

# Sphere Insertion and PMF


# Quinn Alexander Besford
# Independently Initiated 17/02/2016


# Initiate loop for n simulations

 
for n in `seq 1 1000`
do
# Generate random translation (in x,y, or z, or a mix of all)

	template_x=2500 
	template_y=2500  
	template_z=2500  
	


# random between 100 and 2500 bohr

	ran_x=$[50+$[RANDOM% 100]]  
	ran_y=$[50+$[RANDOM% 100]] 
	ran_z=$[50+$[RANDOM% 100]] 

	new_x=$(($ran_x+$template_x))
	new_y=$(($ran_y+$template_y))
	new_z=$(($ran_z+$template_z))

	displacement=$(echo "scale=2;sqrt(((($template_x-$new_x)^2)+(($template_y-$new_y)^2)+(($template_z-$new_z)^2)))" | bc) 
#	echo "$displacement"

        awk -v var1="$ran_x" -v var2="$ran_y" -v var3="$ran_z" '{print ($1+var1) "\t" ($2+var2) "\t" ($3+var3)}' TEMPLATE.xyz > NewSphere.xyz

# Do some cleaning
	rm TwoSphereCoordinates.xyz
	rm NewRunTemplate.input

#Find atoms

	sed -n '1,512 p' TEMPLATE.xyz > O_1
	sed -n '1,512 p' NewSphere.xyz > O_2
	sed -n '513,1536 p' TEMPLATE.xyz > H_1
	sed -n '513,1536 p' NewSphere.xyz > H_2

	cat O_1 > TwoSphereCoordinates.xyz
	cat O_2 >> TwoSphereCoordinates.xyz
	cat H_1 >> TwoSphereCoordinates.xyz
	cat H_2 >> TwoSphereCoordinates.xyz

# Choose selected areas from old template input
	head -n 3199 TWO_SPHERE_TEMPLATE.input > NewRunTemplate.input

# Add the new coordinates on with the velocities appended
	cat TwoSphereCoordinates.xyz >> NewRunTemplate.input
	cat velocities_template >> NewRunTemplate.input

# cleanup

	rm O_1 
	rm O_2
	rm H_1
	rm H_2
	rm NewSphere.xyz
	rm TwoSphereCoordinates.xyz

# Move into the active directory

	cp NewRunTemplate.input ActiveRuns

	cd ActiveRuns

# Run the simulation

	toymd-ifort-opt.x < NewRunTemplate.input > NewRunTemplate.output 
	
	./select.py th

#	rm displacementlist.txt
#	for i in `seq 1 21` #This is the length of the current set thermo output
#	do
#		echo $displacement >> displacementlist.txt
#	done

#	awk '{print $3 "\t" $5 "\t" $6 "\t" $7 "\t" $8 "\t" $9}' NewRunTemplate.th > FinalFormat_th.txt 

	awk '{sum_KE+=$6;sumsq_KE+=$6*$6}END{print sum_KE/NR "\t" sqrt(sumsq_KE/NR-(sum_KE/NR)^2)}' NewRunTemplate.th > KE_Final.txt
        awk '{sum_U+=$7;sumsq_U+=$7*$7}END{print sum_U/NR "\t" sqrt(sumsq_U/NR-(sum_U/NR)^2)}' NewRunTemplate.th > U_Final.txt
        awk '{sum_H+=$8;sumsq_H+=$8*$8}END{print sum_H/NR "\t" sqrt(sumsq_H/NR-(sum_H/NR)^2)}' NewRunTemplate.th > H_Final.txt

	echo $displacement > displacementlist.txt
	paste displacementlist.txt KE_Final.txt U_Final.txt H_Final.txt >> End_Thermodynamics.txt
	
	rm KE_Final.txt
	rm U_Final.txt
	rm H_Final.txt 	
	rm End_Thermodynamics.th > /dev/null 2>&1
	rm Latest_th.txt > /dev/null 2>&1
	rm displacementlist.txt
	rm FinalFormat_th.txt
	rm NewRunTemplate.input 
	rm NewRunTemplate.output 
	rm NewRunTemplate.th 


	cd ..

# Output
#
# displacement      |        thermodynamics			|   Pressure 
#     r                 steps    nfree  KE/nfree   U/nfree   H/nfree
#

done

echo "------------------------------------------------------------------------"
echo "Job Done"
echo "------------------------------------------------------------------------"




















#!/bin/bash

for dir in Distance*;
do
	cd $dir
	paste AveragesumprFrame* > WorkingAverage
	echo "For $dir:"
	awk '{for(i=1;i<=NF;i++)x+=$i;print x/NF}' WorkingAverage
	echo ".........................."
	cd ..
done






#!/bin/bash
# Calculate the Kirkwood g-factor from simulations
# Author: Quinn A. Besford
# Email: quinn.besford@gmail.com
# Date of last edit: 19/01/2014

# Welcome
clear
echo "================================================================"
echo "Dipole-builder 1.0"
echo "================================================================"


# User inputs

echo "What is the charge of the original species?:"
read CHARGEO
echo "What is the charge of the formed species?: "
read CHARGE

echo "================================================================"
echo "Calculating G_k"

# First let us ensure mole0.0 has x1.0 directory label
if [ -d "mole0.0" ]
then
      cd mole0.0
      mv x* x1.0 >/dev/null 2>&1
      cd ..
fi


for dir1 in mole*
do

# Now we must calculate |M|^2 for all mole fractions
 	cd $dir1
	cd x1.0
#	cd Longer
	cd Longer	
	WORKINGDIRECTORY=`pwd`
        

        FRACTION=${dir1#mole}
        TOTAL=$( echo "scale=0; $FRACTION*1000" | bc )
        TOTAL=$( printf "%.0f" $TOTAL )

        
        S1=$((($TOTAL/2)))
        S2=$((500-$S1))

# Move into comoda and adjust dipole-builder

        cd ~/comodaOLD/src/
        make veryclean >/dev/null
        cd dipole

        sed -i "10s/.*/  A = m_system_add_molec_spec (s, \"A\", $S1);/" calc-M2-stockmayer.c
        sed -i "11s/.*/  m_system_add_atom_spec (s, \"A\", \"A\", \"A\", 1, 0, 0, $CHARGE);/" calc-M2-stockmayer.c

        sed -i "12s/.*/  B = m_system_add_molec_spec (s, \"B\", $S2);/" calc-M2-stockmayer.c
        sed -i "13s/.*/  m_system_add_atom_spec (s, \"B\", \"B\", \"B\", 1, 0, 0, $CHARGEO);/" calc-M2-stockmayer.c

        sed -i "14s/.*/  C = m_system_add_molec_spec (s, \"C\", $S1);/" calc-M2-stockmayer.c
        sed -i "15s/.*/  m_system_add_atom_spec (s, \"C\", \"C\", \"C\", 1, 0, 0, -$CHARGE);/" calc-M2-stockmayer.c

        sed -i "16s/.*/  D = m_system_add_molec_spec (s, \"D\", $S2);/" calc-M2-stockmayer.c
        sed -i "17s/.*/  m_system_add_atom_spec (s, \"D\", \"D\", \"D\", 1, 0, 0, -$CHARGEO);/" calc-M2-stockmayer.c

        cd ..
        make dipole >/dev/null

# Change back to working directory

        cd $WORKINGDIRECTORY

# Tidy up       
        rm *.x >/dev/null 2>&1
        rm *.xyz >/dev/null 2>&1

        select.py x
#	select.py cel
        mkXYZ.py stockmayer.nvt.x A:$S1 B:$S2 C:$S1 D:$S2 --nvt 8.54988 8.54988 8.54988 >/dev/null

# Build the dipoles (note the c-script puts output as dipole.out in working directory)

        ~/comodaOLD/bin/calc-M2-stockmayer < stockmayer.nvt.x.xyz

        M=`awk '{ total += $1; count+=1 } END { print total/count }' dipole.out`
        d1=`awk '{ total += $2; count++ } END { print total/count }' dipole.out`
        d2=`awk '{ total += $3; count++ } END { print total/count }' dipole.out`
 	d1moment=`awk '{ total+=$4;count+=1}END{print total/count}' dipole.out`
	       
	dipole1=$(echo "scale=2;$S1*$d1" | bc)
	dipole2=$(echo "scale=2;$S2*$d2" | bc)
	zero=0
		
	if [ $dipole1 -eq $zero ] >/dev/null 2>&1
	then
	
		gk=$( echo "scale=4; $M/$dipole2" | bc) >/dev/null 2>&1 
        
	elif [ $dipole2 -eq $zero ] >/dev/null 2>&1
	then
	
		 gk=$( echo "scale=4; $M/$dipole1" | bc) >/dev/null 2>&1  
	
	else
	
		gk=$( echo "scale=4; $M/($dipole1+$dipole2)" | bc) >/dev/null 2>&1
        fi
        

#	M1=`awk '{ total += $4; count++ } END { print total/count }' dipole.out`
#	M2=`awk '{ total += $5; count++ } END { print total/count }' dipole.out`
#	echo "$dir1 = $gk"
#	echo "M1    = $M1"
#	echo "M2    = $M2"       
       
	echo "mole$FRACTION = $gk   $d1moment"	

	cd ..
 	cd ..
        cd ..
 #       cd ..

done	
	echo "=============================================================="
#	cd mole1.0	
#	cd x1.0
#	dipole1end=`awk '{ total += $2; count++ } END { print total/count }' dipole.out`
#	echo "Species 1 mu^2: $dipole1end"
	
#	cd ..
#	cd ..
	
##	cd mole0.1
#	cd x1.0	
#	dipole2end=`awk '{ total += $3; count++ } END { print total/count }' dipole.out`
 #       echo "Species 2 mu^2: $dipole2end"
#	cd ..
#	cd ..

	
echo "=============================================================="

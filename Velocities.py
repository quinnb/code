#!/usr/bin/python
import os
os.system('clear')
import sys
import numpy as np
import random
# Generate velocities for toyMD input
# Samples from a normal distribution with sigma=1

# Quinn A. Besford 
# quinn.besford@gmail.com
# 04/07/2014
particles = raw_input("Number of sites:")
particles = int(particles)

XYZ = []
X=np.random.normal(0.0,0.1,particles)
Y=np.random.normal(0.0,0.1,particles)
Z=np.random.normal(0.0,0.1,particles)
for x in range(0,particles):
	XYZ.append(X[x])
	XYZ.append(Y[x])
	XYZ.append(Z[x])
outveloc=np.array(XYZ).reshape(-1,3)

f=open("velocities.tsv","w")
for i in range(len(outveloc)):
	f.write("\t".join([str(x) for x in outveloc[i]])+"\n")
f.close()

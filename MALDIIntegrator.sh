#!/bin/bash
# Purpose: Do a full thermodynamic integration for all mole fractions of mixed diatoms
# Author: Quinn A. Besford
# Email: quinn.besford@gmail.com
# Date: 30/10/2013

clear
# User inputs
echo "====================================================================="
echo "PHISORT 3.0"
echo "NOTE: This script assumes species 2 and 4 are changing!"
echo "====================================================================="

#echo "How many changed species are there (total of positive and negative):"
#read TOTAL

echo "What is the charge of the original species?:"
read CHARGE1
echo "What is the charge of the forming species?:"
read CHARGE2

#echo "How long is the simulation output (nstep/outskip)?:"
#read STEPS
#echo "How many sites are there?:"
#read SITES

# What percentage of runs do we drop, default is 60%
EQ=20
read -e -i $EQ -p "What percentage of runs do we drop: " input
EQ="${input:-$EQ}"

STEPS=200
read -e -i $STEPS -p "How many nstep/nskip?:" input
STEPS="${input:-$STEPS}"

SITES=1000
read -e -i $SITES -p "How many sites?:" input
SITES="${input:-$SITES}"
      
echo "===================================================================="
echo "Calculating..."
# Setup a loop to iterate over every mole fraction for the given conditions
# and skip mole fraction 0.0
for dir1 in mole*
do
        FRACTION=${dir1#mole}
        TOTAL=$( echo "scale=0;$FRACTION*1000" | bc)
	TOTAL=$( printf "%.0f" $TOTAL )
        if [ $TOTAL -eq 0 ]
        then
                continue
        fi
	cd $dir1
	echo "===================================================================="
	echo "$dir1"

# Setup a loop as to iterate over every lambda in the specific mole fraction
	if [ -f MoleResults.txt ]
	then
		rm MoleResults.txt
	fi

	for dir2 in x*
	do
# We ignore lambda 0.2, 0.4, 0.6 and 0.8, as the data is normally good enough to skip these.	
#		if [ "$dir2" == "x0.2" -o "$dir2" == "x0.4" -o "$dir2" == "x0.6" -o "$dir2" == "x0.8" ]
#		then
#			continue
#		fi
		
		cd $dir2
		if [ -d "TempAvDump" ]
		then
			rm -r TempAvDump
		fi

		mkdir TempAvDump
# For now couple in the phi script
		if [ -f *.phi ]
		then
			rm *.phi
		fi
#	        select.py phi
                grep "phi" *.output > stockmayer.nvt.phi
# Sort the phi output file and dump the initial (non-equilibrated)
# data that is not desired

# What is our higher bound of data?
		HB=$((($SITES*$STEPS)+$SITES))
# What is our lower bound of data?
		PERCENTAGE=$(echo "scale=2;$EQ/100" | bc)
		LB=$(echo "scale=2;(((($HB/$SITES)-1)*$PERCENTAGE)*$SITES)+1" | bc)
		LB=$( printf "%0.0f" $LB)

# Pull equilibrated data out (according to input $EQ)
		sed $LB,$HB!d stockmayer.nvt.phi > PhiTot

# Throw all data except phi
		awk '{print $3}' PhiTot > PhiTotOnly
# A Handle to separate equilibrated data into runs of length $SITES
		RUNS=$((($HB-($LB-1))/$STEPS))
# Separate equilibrated data into new files of length $SITES
# with phi printed
		for b in `seq 1 $RUNS`; do
        		FIRST=$((($b*$SITES)+1))
       			END=$((($b*$SITES)+1+($SITES-1)))
       	       		sed $FIRST,$END!d PhiTotOnly > ./TempAvDump/$b.txt
		done

# Glue all sister sites together
		cd TempAvDump
		paste *.txt > AverageOut


# Take sites in run 1 and average over equilibrated sister sites (up to $RUNS)
		awk '{x=0;for(c=1;c<=NF;c++)x=x+$c;Avg=x/c;print Avg}' AverageOut > WorkingAverage


# A few handles to assist numeric scaling across lists
		OS=$(($TOTAL/2))
# Separate out the atoms making up the diatom (positive and negative atoms)
# 1st SHAKE constrained species
		FirstSpecies=$((501-$OS))
		sed $FirstSpecies,500!d WorkingAverage > Species2Positive

# Handles to get 2nd SHAKE constrained species
		SecSpecies=$(((1001-$OS)))
		SecSpeciesLast=$((1000))
# 2nd SHAKE constrained species
		sed $SecSpecies,$SecSpeciesLast!d WorkingAverage > Species2Negative


# Negative charges (Charge Difference Negative (CDN))
		CDN=$(echo "scale=1;($CHARGE2)-(-$CHARGE1)" | bc)
# For this variable to be understood by awk we must redeclare it
# as a dummy variable within the awk framework:
		awk -v r=$CDN '{print $1*r}' Species2Negative > ../PhiNegative

# Positive charges (CDP)
        	CDP=$(echo "scale=1;$CHARGE2-$CHARGE1" | bc)
        	awk -v r=$CDP '{print $1*r}' Species2Positive > ../PhiPositive


		cd ..
# Now lets calculate what dH/dLambda is
		paste PhiNegative PhiPositive > Combo
		awk '{print $1+$2}' Combo > Combo2
		
		echo "dH / dLambda for $dir2 is: "
		awk '{sum+=$1}END{print sum}' Combo2
		awk '{sum+=$1}END{print sum}' Combo2  >> ../MoleResults.txt

		# Clean up
                rm -r TempAvDump
		rm Combo
                rm PhiTot
                rm PhiTotOnly
		rm Combo2
 		rm PhiPositive
		rm PhiNegative
		cd ..
	done
# Couple in integrate.py
		echo "===================================================================="
		integrate.py
		echo "===================================================================="
	cd ..
done


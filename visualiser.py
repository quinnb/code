#!/usr/bin/python
import os
os.system('clear')
import sys
import numpy as np
from numpy import *
import matplotlib.pyplot as plt

datafirst = np.genfromtxt("surfacetensionSmallGrid",delimiter=" ")
data = np.delete(datafirst,0)
Xtemp = []
for i in range(0,len(data)):
	Xtemp.append(i)
x = np.array(Xtemp)

def movingaverage (values, window):
    weights = np.repeat(1.0, window)/window
    sma = np.convolve(values, weights, 'valid')
    return sma

yMA = movingaverage(data,3)

plt.plot(x[len(x)-len(yMA):],yMA)
plt.axhline(y=sum(data)/len(data), xmin=0, xmax=len(x), linewidth=2)
plt.axhline(y=0, xmin=0, xmax=len(x),linewidth=2, color = 'k')
plt.title('Moving average of surface tension')
plt.xlabel('frames')
plt.ylabel('gamma')
plt.show()


#!/usr/bin/python
'''
Converts pdb files to xyz files suitable for toyMD input
Quinn A. Besford
quinn.besford@gmail.com

#######################
NOTES:
This is currently configured for SPCE water with an input of
"waterOutput.pdb.
This can easily be adjusted for any system.
#######################
'''
import os
os.system('clear')
import sys
import numpy as np

Box = raw_input("Box dimensions:")
Box = float(Box)

file = open('waterOutput.pdb', 'rb')
table = [row.strip().split('\t') for row in file]

coordinates = []
for i in range(5,1541):
	coordinates.append(table[i])

H1 = []
H2 = []
O = []
for i in range(0,512):
	H1.append(coordinates[i*3])
	H2.append(coordinates[1+i*3])
	O.append(coordinates[2+i*3])

H1xyz = []
H2xyz = []
Oxyz = []
for i in range(0,512):
	TempH1=str(H1[i])
	TempH2=str(H2[i])
	TempO=str(O[i])
	H1xyz.append(TempH1[34:56])
	H2xyz.append(TempH2[34:56])
	Oxyz.append(TempO[34:56])

Total = (Oxyz+H1xyz+H2xyz)
FTot = []
FinalTotDone = []
for i in range(0,len(Total)):
	FTot.append([float(x) for x in Total[i].split()])

Final = []
for i in range(0,len(FTot)):
	xyz = []
	for j in range(0,3):
		if FTot[i][j]<0:
			Temp = (Box+FTot[i][j])*(1/0.529)
		else:
			TEMP = FTot[i][j]*(1/0.529)
		xyz.append(TEMP)
	Final.append(xyz)
f=open("NewCoordinates.tsv","w")
for i in range(0,len(Final)):
	f.write("\t".join([str(x) for x in Final[i]])+"\n")
f.close()




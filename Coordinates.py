#!/usr/bin/python
import os
os.system('clear')
import sys
import numpy as np
import random
import readline
# Quinn A. Besford 
# quinn.besford@gmail.com
# 04/07/2014

def stderr_input(prompt=None):
	if prompt:
		sys.stderr.write(str(prompt))
	return raw_input()

# Generate a Z matrix for toyMD
Density = stderr_input("Number density:")
Density = float(Density)
XX = stderr_input("X:")
XX = 1+int(XX)
YY = stderr_input("Y:")
YY = 1+int(YY)
ZZ = stderr_input("Z:")
ZZ = 1+int(ZZ)

def cartesian(arrays, out=None):

	arrays = [np.asarray(x) for x in arrays]
	dtype = arrays[0].dtype

	n = np.prod([x.size for x in arrays])

	if out is None:
		out = np.zeros([n,len(arrays)],dtype=dtype)
	m = n / arrays[0].size
	out[:,0] = np.repeat(arrays[0], m)
	if arrays[1:]:
		cartesian(arrays[1:], out=out[0:m,1:])
		for j in xrange(1, arrays[0].size):
            		out[j*m:(j+1)*m,1:] = out[0:m,1:]
	return out
outcart=cartesian(([range(1,XX)],[range(1,YY)],[range(1,ZZ)]))

# Generate the correct number density
Dendelete=Density*len(outcart)
Delete=len(outcart)-Dendelete
Delete=int(Delete)

for i in range(0,Delete):
	D=np.random.randint(0,len(outcart))
	Del=np.delete(outcart,D,0)
	outcart=Del
print "There are"
print len(outcart)
print "in your simulation"
f=open("coordinates.tsv","w")
for i in range(len(outcart)):
	f.write("\t".join([str(x) for x in outcart[i]])+"\n")

f.close()



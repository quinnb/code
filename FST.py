#!/usr/bin/python
# Purpose: Take the thermodynamics from a QHNC calculation and calculate the change in free energy (Beta DA)
# Author: Quinn A. Besford
# Email: quinn.besford@gmail.com
# Date: 29/01/2014

# Setup
import os
os.system('clear')
import sys
import numpy as np
from numpy import *
from scipy import integrate
from scipy.integrate import trapz
from scipy.integrate import quad

print "****************************************************************************************"
print "Fluid Structure Thermodynamics (FST)"
print "****************************************************************************************"
# Some user inputs
temp1 = raw_input("Min 1/kT you are interested in:? ")
temp1 = float(temp1)
temp2 = raw_input("Max 1/kT you are interested in?: ")
temp2 = float(temp2)
print "****************************************************************************************"
# Add the potentials between 11, 12, and 22 (if they exist)

with open('thermo.out') as fd:
        line = fd.readline()
Size = len(line.split())

if Size==24:
        L11Internal = []
        L12Internal = []
        L22Internal =[]
        L11Dipole = []
        L12Dipole = []
        L22Dipole = []
        LBeta = []
	Lgk = []
	Lepsilon = []
	
        with open('thermo.out') as fd:
                fd.readline()
                for line in fd:
                        L11Internal.append(line.split()[3])
                        L12Internal.append(line.split()[4])
                        L22Internal.append(line.split()[5])
                        L11Dipole.append(line.split()[15])
                        L12Dipole.append(line.split()[16])
                        L22Dipole.append(line.split()[17])
                        LBeta.append(line.split()[1])
			Lgk.append(line.split()[22])
			Lepsilon.append(line.split()[23])
        Length = len(LBeta)
        L11Ifloat = []
        L12Ifloat = []
        L22Ifloat = []
        L11Dfloat = []
        L12Dfloat = []
        L22Dfloat = []
        LInternal = []
        LDipole = []
        for x in range (0,Length):
                L11Ifloat.append(float(L11Internal[x]))
                L12Ifloat.append(float(L12Internal[x]))
                L22Ifloat.append(float(L22Internal[x]))
                L11Dfloat.append(float(L11Dipole[x]))
                L12Dfloat.append(float(L12Dipole[x]))
                L22Dfloat.append(float(L22Dipole[x]))
        for x in range (0,Length):
                LInternal.append(L11Ifloat[x]+L12Ifloat[x]+L22Ifloat[x])
                LDipole.append(L11Dfloat[x]+L12Dfloat[x]+L22Dfloat[x])
        Internal = []
        Dipole = []
        Beta = []
	gk = []
	epsilon = []	
        for x in range (0,Length):
                Internal.append(float(LInternal[x]))
                Dipole.append(float(LDipole[x]))
                Beta.append(float(LBeta[x]))
		gk.append(float(Lgk[x]))
		epsilon.append(float(Lepsilon[x]))
        Internal = np.array(Internal)
        Dipole = np.array(Dipole)
	
elif Size==12:
        LInternal = [ ]
        LDipole = [ ]
        LBeta = [ ]
	Lgk = []
	Lepsilon = []

        with open('thermo.out') as fd:
                fd.readline()
                for line in fd:
                        LInternal.append(line.split()[3])
                        LDipole.append(line.split()[7])
                        LBeta.append(line.split()[1])
			Lgk.append(line.split()[10])
			Lepsilon.append(line.split()[11])

        Length = len(LInternal)
        Internal = []
        Dipole = []
        Beta = []
	gk = []	
	epsilon = []
        for x in range (0,Length):
                Internal.append(float(LInternal[x]))
                Dipole.append(float(LDipole[x]))
                Beta.append(float(LBeta[x]))
		gk.append(float(Lgk[x]))	
		epsilon.append(float(Lepsilon[x]))
	Internal = np.array(Internal)
        Dipole = np.array(Dipole)
	Length = len(Beta)
else:
        print "ERROR: Can only handle Homogenous or Binary systems"
        exit()

Position1 = (np.abs([Beta[x] - temp1 for x in range(len(Beta))])).argmin()


Position2 = (np.abs([Beta[x] - temp2 for x in range(len(Beta))])).argmin()
def find_nearest(Beta, temp):
        idx = (np.abs([Beta[x] - temp for x in range(len(Beta))])).argmin()
        return Beta[idx]
Target2 = (find_nearest(Beta, temp2))
Target1 = (find_nearest(Beta, temp1))

LDU = []
Cv_B = []
DS = []
DA = []

for x in range (0,Length):

        LDU.append(Internal[x]+Dipole[x])
LDU = np.array(LDU)
DSTest = []

NewLength = len(LDU)-1


for x in range (1,NewLength):

        Delta = ((Beta[(x+1)]) - (Beta[(x-1)]))

	Cv = - (Beta[x]**2)*((LDU[(x+1)] - LDU[(x-1)]) / Delta)

        CvOnBeta =- Cv/Beta[x]
        Cv_B.append(CvOnBeta)

Cv_B = np.array(Cv_B)

data_str="{"
for x in range(0,len(Cv_B)):
	data_str+="{"+str(Beta[x])+","+str(Cv_B[x])+"}"
	if (x+1<len(Cv_B)):
		data_str+=","
data_str+="}"

cmd_str="math << END | grep Out | awk '{print $2}'\n"
cmd_str+="ToPlot = "
cmd_str+=data_str
cmd_str+=";\n"
cmd_str+='f = NonlinearModelFit[ToPlot, {a/(\[Beta])^c + a0}, {a, a0, c}, \[Beta]];'
cmd_str+="\n"

integral=[]
for x in range(Position1,Position2):
	new_cmd_str=cmd_str
	new_cmd_str+='Integrate[f[\[Beta]],{\[Beta],0,'+str(Beta[x])+'}]'
	new_cmd_str+="\n"
	new_cmd_str+="END"
	temp_int=float(os.popen(new_cmd_str).read())
#	print str(x)+"\t"+str(temp_int)
	integral.extend([temp_int])

DS = np.array(integral)

Length = len(DS)
DU = []

for x in range (Position1,Position2):
	DU.append(LDU[x])
np.array(DU)
ShortBeta = Beta[Position1:Position2]
for x in range (0,len(DU)):

	LDA = DU[x] - (1/ShortBeta[x])*DS[x]
	DA.append(LDA)

DA = np.array(DA)

# Cross fingers
Fg = -(3/2)*(1/Beta[Position2])*(gk[Position2]**2)*(((epsilon[Position2]-1)**2)/(((2*epsilon[Position2]+1)**2)))

# Functions to print
BetaPrint = []
InternalPrint = []
DipolePrint = []
EntropyPrint = []

Beta = np.array(ShortBeta)

for x in range(0,len(ShortBeta)):
	BetaPrint.append(float(Beta[x]))
	InternalPrint.append(float(Internal[x]))
	DipolePrint.append(float(Dipole[x]))
	EntropyPrint.append(float(DS[x]*(1/Beta[x])))

# Write out the DA
# ExportResults = np.array([DA,DU,DS,Cv_B,InternalPrint,DipolePrint,EntropyPrint])
# ExportResults = ExportResults.transpose()


# Results!
print "kT	     |", 
print Target2
print "Beta DA	     |",
print DA[len(DA)-1]
print "Beta DU      |",
print DU[len(DA)-1]
print "TDS/k        |",
print DS[len(DA)-1]*(1/ShortBeta[len(DA)-1])
print "gK    	     |",
print gk[Position2]
print "epsilon	     |",
print epsilon[Position2]
print "Fg    	     |",
print Fg

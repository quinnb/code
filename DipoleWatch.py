#!/usr/bin/python
import os
os.system('clear')
import sys
import numpy as np

Length = int(1536)
X = []
file = open('XYZ.txt','rb')
table = [row.strip().split('\t') for row in file]
X = np.array(table)
Pos = len(X)/(Length+2)
Xeq = []
TotalSimulation = []
for q in range(0,Pos-2):
	for x in range((q)*(Length+2)+2,(q+1)*(Length+2)):
        	Xeq.append(X[x])

	XX= []
	XFinal = []
	YY = []
	YFinal = []
	ZZ = []
	ZFinal = []

# Find the data we want
	for x in range(0,Length-1):
        	XTEMP= Xeq[x][1]
		XX.append(XTEMP)
       		for item in XX:
  			XXX=float(item)
        	XFinal.append(XXX)
	for y in range(0,(Length-1)):
        	YTEMP= Xeq[y][2]
        	YY.append(YTEMP)
        	for item in YY:
                	YYY=float(item)
        	YFinal.append(YYY)
	for z in range(0,(Length-1)):
        	ZTEMP= Xeq[z][3]
        	ZZ.append(ZTEMP)
        	for item in ZZ:
                	ZZZ=float(item)
        	ZFinal.append(ZZZ)
# Find the specfic atoms
	OxygenLen=(Length/3)
	HydrogenLen1=(Length/3)+OxygenLen
	HydrogenLen2=(Length/3)+OxygenLen+HydrogenLen1

#Calculate the new coordinate (dipole)
	OX = []
	H1X = []
	H2X = []
	OY = []
	H1Y = []
	H2Y = []
	OZ = []
	H1Z = []
	H2Z = []

	for x in range(512,1023):
		H1X.append(XFinal[x])
		H2X.append(XFinal[(x+512)])	
		OX.append(XFinal[(x-512)])
	NewX = []
	for x in range(0,511):
		NewXTemp = (H1X[x]+H2X[x])/2
		NewX.append(NewXTemp)
	for y in range(512,1023):
        	H1Y.append(YFinal[y])
        	H2Y.append(YFinal[(y+512)])
		OY.append(YFinal[(y-512)])
	NewY = []
	for y in range(0,511):
        	NewYTemp = (H1Y[y]+H2Y[y])/2
       		NewY.append(NewYTemp)
	for z in range(512,1023):
        	H1Z.append(ZFinal[z])
       		H2Z.append(ZFinal[(z+512)])
		OZ.append(ZFinal[(z-512)])
	NewZ = []
	for z in range(0,511):
        	NewZTemp = (H1Z[z]+H2Z[z])/2
        	NewZ.append(NewZTemp)
# Array the 3D coordinates together
	NewCoordinates = []
	for i in range(0,511):
		XYZTemp = (NewX[i],NewY[i],NewZ[i])
		NewCoordinates.append(XYZTemp)
	NewCoord = np.array(NewCoordinates)	
	OTot = []
	for i in range(0,511):
		OtotTEMP = (OX[i],OY[i],OZ[i])
		OTot.append(OtotTEMP)
	Preamble = []
	for i in range(0,2):
		PreambleTemp = X[i]
		Preamble.append(PreambleTemp)
	TotalFrame = (Preamble+OTot+NewCoordinates)
	TotalSimulation.append(TotalFrame)

Finale=open("Dipole.tsv","w")
for i in range(len(TotalSimulation)):
	Finale.write("\t".join([str(x) for x in TotalFrame[i]])+"\n")
Finale.close()





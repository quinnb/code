#!/usr/bin/env Rscript
d<-scan("stdin", quiet=TRUE, skip=1)
cat(mean(d), sep="\n")
cat(sd(d), sep="\n")
cat(length(d), sep="\n")
cat(sd(d)/sqrt(length(d)), sep="\n")



#!/bin/bash

# grep -G '^\s12\.5' frame*

# Make our lists

rm d*

for x in frame*
do
	grep -G '^\s*2.5' $x >> da2	
	grep -G '^\s*7.5' $x >> db7
	grep -G '^\s*12.5' $x >> dc12
	grep -G '^\s*17.5' $x >> dd17
	grep -G '^\s*22.5' $x >> de22
	grep -G '^\s*27.5' $x >> df27
	grep -G '^\s*32.5' $x >> dg32
	grep -G '^\s*32.5' $x >> dh37
	grep -G '^\s*42.5' $x >> di42
	grep -G '^\s*47.5' $x >> dj47
	grep -G '^\s*52.5' $x >> dk52
	grep -G '^\s*57.5' $x >> dl57
	grep -G '^\s*62.5' $x >> dm62
	grep -G '^\s*67.5' $x >> dn67
	grep -G '^\s*72.5' $x >> do72
	grep -G '^\s*77.5' $x >> dp77
	grep -G '^\s*82.5' $x >> dq82
	grep -G '^\s*87.5' $x >> dr87
	grep -G '^\s*1\s' $x >> ds0
done

rm results
rm sdresults
rm totalresults

for x in  d*
do
	awk '{print $2}' $x | NewStats.r >> results 
#	awk '{print $2}' $x | Stdev.r >> sdresults
done

#paste results sdresults > totalresults


rm d*
#rm results
rm sdresults


#echo d0
#awk '{print $

#for x in d*
#do
#	awk '{sum+=$2}END{print sum}' $x > sum$x 
#done


#rm d*

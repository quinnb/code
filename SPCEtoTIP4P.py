#!/usr/bin/python

# Quinn A. Besford
# 2014

import os
os.system('clear')
import sys
import numpy as np
from math import *

Length = int(1536)
X = []
file = open('XYZ.txt','rb')
table = [row.strip().split('\t') for row in file]
X = np.array(table)
Pos = len(X)/(Length+2)

Xeq = []
TotalSimulation = []
for q in range(2,1538):
        Xeq.append(X[q])		

XX= []
XFinal = []
YY = []
YFinal = []
ZZ = []
ZFinal = []

# Find the data we want
for x in range(0,Length):
        XTEMP= Xeq[x][1]
	XX.append(XTEMP)
       	for item in XX:
  		XXX=float(item)
        XFinal.append(XXX)
for y in range(0,(Length)):
        YTEMP= Xeq[y][2]
        YY.append(YTEMP)
        for item in YY:
                YYY=float(item)
        YFinal.append(YYY)
for z in range(0,(Length)):
        ZTEMP= Xeq[z][3]
        ZZ.append(ZTEMP)
        for item in ZZ:
                ZZZ=float(item)
        ZFinal.append(ZZZ)
# Find the specfic atoms
OxygenLen=(Length/3)
HydrogenLen1=(Length/3)+OxygenLen
HydrogenLen2=(Length/3)+OxygenLen+HydrogenLen1

#Calculate the new coordinate (dipole)
OX = []
H1X = []
H2X = []
OY = []
H1Y = []
H2Y = []
OZ = []
H1Z = []
H2Z = []

for x in range(512,1024):
	H1X.append(XFinal[x])
	H2X.append(XFinal[(x+512)])	
	OX.append(XFinal[(x-512)])
# Make lone pair

NewX = []
for x in range(0,512):
	NewXTemp = ((H1X[x]+H2X[x])/2)
	NewX.append(NewXTemp)
for y in range(512,1024):
        H1Y.append(YFinal[y])
        H2Y.append(YFinal[(y+512)])
	OY.append(YFinal[(y-512)])
NewY = []
for y in range(0,512):
        NewYTemp = ((H1Y[y]+H2Y[y])/2)
       	NewY.append(NewYTemp)
for z in range(512,1024):
        H1Z.append(ZFinal[z])
       	H2Z.append(ZFinal[(z+512)])
	OZ.append(ZFinal[(z-512)])
NewZ = []
for z in range(0,512):
        NewZTemp = ((H1Z[z]+H2Z[z])/2)
        NewZ.append(NewZTemp)
# Array the 3D coordinates together
NewCoordinates = []
for i in range(0,512):
	XYZTemp = [NewX[i],NewY[i],NewZ[i]]
	NewCoordinates.append(XYZTemp)
NewCoord = np.array(NewCoordinates)	

# Find M distance between mid-point (H-H) and O
for i in range(0,512):
	DM =((OX[i]-NewX[i])**2+(OY[i]-NewY[i])**2+(OZ[i]-NewZ[i])**2)**(1/2)
	D = ((OX[i]-H1X[i])**2+(OY[i]-H1Y[i])**2+(OZ[i]-H1Z[i])**2)**(1/2)

# Find coordinates of the lone pair
# the 0.738 factor is to indicate position towards O. when factor =1 we are at O, when 
# equals 0 we are at the mid-point (H-H)
XMNew = []
YMNew = []
ZMNew = []
for i in range(0,512):
	XM = ((1-0.740196292)*NewX[i])+(0.740196292*OX[i])
	YM = ((1-0.740196292)*NewY[i])+(0.740196292*OY[i])
	ZM = ((1-0.740196292)*NewZ[i])+(0.740196292*OZ[i])
	XMNew.append(XM)
	YMNew.append(YM)
	ZMNew.append(ZM)
NewMCoordT = []
for i in range(0,512):
	MTemp = [XMNew[i],YMNew[i],ZMNew[i]]
	NewMCoordT.append(MTemp)
NewMCoord = list(NewMCoordT)

# Adjust the Hydrogens'
H1XNew = []
H1YNew = []
H1ZNew = []

H2XNew = []
H2YNew = []
H2ZNew = []

theta = (104.52/2)*(pi/180)
r  = 0.9572

#Adjust towards H-H
for i in range(0,512):
	
	VectorOH1 = [(H1X[i]-OX[i]) , (H1Y[i]-OY[i]) , (H1Z[i]-OZ[i])]
	VectorOH2 = [(H2X[i]-OX[i]) , (H2Y[i]-OY[i]) , (H2Z[i]-OZ[i])]
	DotOH1 = VectorOH1[0]*VectorOH1[0]+VectorOH1[1]*VectorOH1[1]+VectorOH1[2]*VectorOH1[2]

	UnitOH1 = [((DotOH1)**(-0.5))*VectorOH1[0], ((DotOH1)**(-0.5))*VectorOH1[1],(DotOH1)**(-0.5)*VectorOH1[2]]
	
	alpha = [(XMNew[i]-OX[i]), (YMNew[i]-OY[i]), (ZMNew[i]-OZ[i])]	
	alphaDot = alpha[0]*alpha[0]+alpha[1]*alpha[1]+alpha[2]*alpha[2]
	alphahat = [((alphaDot)**(-0.5))*alpha[0], ((alphaDot)**(-0.5))*alpha[1], ((alphaDot)**(-0.5))*alpha[2]]
	
	kAlphaDot = VectorOH1[0]*alpha[0] + VectorOH1[1]*alpha[1] + VectorOH1[2]*alpha[2]
	beta = [ VectorOH1[0] - (kAlphaDot*alpha[0]), VectorOH1[1] - (kAlphaDot*alpha[1]), VectorOH1[2] - (kAlphaDot*alpha[2]) ]
	betadot = beta[0]*beta[0] + beta[1]*beta[1] + beta[2]*beta[2]
	betahat = [((betadot)**(-0.5))*beta[0], ((betadot)**(-0.5))*beta[1], ((betadot)**(-0.5))*beta[2]]	

	OH1p =[(r * (cos(theta)*alphahat[0] + sin(theta)*betahat[0])), (r * (cos(theta)*alphahat[1] + sin(theta)*betahat[1])), (r * (cos(theta)*alphahat[2] + sin(theta)*betahat[2]))]

	H1NewVector = [OX[i] +  OH1p[0], OY[i] + OH1p[1], OZ[i] + OH1p[2]]

	print H1NewVector

	H1XNew.append(H1XNTtot)
	H1YNew.append(H1YNTtot)
	H1ZNew.append(H1ZNTtot)
	H2XNew.append(H2XNTtot)
	H2YNew.append(H2YNTtot)
	H2ZNew.append(H2ZNTtot)
NewH1Coord = []
NewH2Coord = []

for i in range(0,512):
	NewH1Temp = [H1XNew[i],H1YNew[i],H1ZNew[i]]
	NewH2Temp = [H2XNew[i],H2YNew[i],H2ZNew[i]]
	NewH1Coord.append(NewH1Temp)
	NewH2Coord.append(NewH2Temp)
#Adjust towards O-H


# Find Oxygen
OTot = []
for i in range(0,512):
	OtotTEMP = [OX[i],OY[i],OZ[i]]
	OTot.append(OtotTEMP)
Preamble = []
for i in range(0,2):
	PreambleTemp = X[i]
	Preamble.append(PreambleTemp)

TotalSimulation = OTot + NewH1Coord + NewH2Coord + NewMCoord

Finale=open("Dipole.tsv","w")
for i in range(len(TotalSimulation)):
	Finale.write("\t".join([str(x) for x in TotalSimulation[i]])+"\n")
Finale.close()




